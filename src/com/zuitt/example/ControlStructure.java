package com.zuitt.example;

import java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args){
        // [SECTION] Java Operators
        // Arithmetic Operators: -, +, /, *, %;
        // Comparison: <, >, <=, >=, ==, !=;
        // Logical: &&, ||, !;
        // Re/assignment operator: =, +=, -=, *=, /=;
        // Increment and decrement: i++, ++i, --i, i--;

        // [SECTION] Selection Control Structure:
        // Statement that allows us to manipulate the flow of the code depending on the evaluation of the condition;
        // Syntax:
        /*
            if (condition) {
                statement
            }
            else if (condition) {
                statement
            }
            else {
                statement
            }
        */

        int num1 = 36;
        if (num1 % 5 == 0) {
            System.out.println(num1 + " is divisible by 5.");
        } else {
            System.out.println(num1 + " is not divisible by 5.");
        }

        // [SECTION] Short Circuiting
        // a technique applicable only to the AND & OR operators wherein if statement/s or other control exists early by ensuring safety operation or for the efficiency


        // [SECTION] Ternary Operator
        int num2 = 24;
        Boolean result = (num2 > 0) ? true : false;
        System.out.println(result);


        // [SECTION] Switch Statement:
        // are used to control the flow of structures that allow one code block out of many other code blocks;

        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int directionValue = numberScanner.nextInt();

        switch (directionValue) {
            case 1:
                System.out.print("North");
                break;
            case 2:
                System.out.print("South");
                break;
            case 3:
                System.out.print("East");
                break;
            case 4:
                System.out.print("West");
                break;
            default:
                System.out.print("Invalid");
        }
    }
}
